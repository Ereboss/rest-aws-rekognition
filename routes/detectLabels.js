var express = require('express');
var router = express.Router();

var AWS = require('aws-sdk');
AWS.config.update({region:'eu-west-1'});  //Set AWS region to ireland (eu-west-1)
var rekognition = new AWS.Rekognition(); //initialise a new AWS Rekognition object

const getBinary = require('../inc/getBinary'); //Import the getBinary function for converting the base64 encoded Image from Ascii to Binary, this is required by AWS.

//-------------------------------------------------//

/* POST users listing. */
router.post('/', function(req, res, next) {
  var token = req.body.token, //initialise all user given parameters
  image = req.body.image,
  maxLabels = req.body.maxLabels,
  minConfidence = req.body.minConfidence;

  var response = {received: true}; //initialise the response object wich is send back to the user



  if(token == "123"){  //check for the right token for user validation
    response.token_accepted = true;
    if(image != undefined || image != ""){
            if(typeof maxLabels == "number"){
                    if(typeof minConfidence == "number"){

                        imageBytes = getBinary(image);  //converting the askii base64 to Binary base64

                        var params = { //setting up the query for the AWS Rekognition service
                        	Image: { /* required */
                        	  Bytes: imageBytes /* Strings will be Base-64 encoded on your behalf */,
                        	},
                        	MaxLabels: maxLabels,
                        	MinConfidence: minConfidence
                        };
                        rekognition.detectLabels(params, function(err, data) { //calling to the AWS Rekognition service
                        	if (err) response.aws_error = err; // an error occurred
                        	else     response.aws_response = data;
                          res.json(response);        // successful response
                        });

                }else {
                    response.error = "minConfidence is " + typeof minConfidence + " but has to be an integer / number";
                    res.json(response);
                }
            }else {
                response.error = "maxLabels is " + typeof maxLabels + " but has to be an integer / number";
                res.json(response);
            }
    }else {
        response.error = "no image given";
        res.json(response);
    }
  }else{
    response.token_accepted = false;
    res.json(response); //the token for user validation was wrong
  }
});

module.exports = router;
