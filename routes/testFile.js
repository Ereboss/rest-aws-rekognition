var express = require('express');
var router = express.Router();

function getBinary(encodedFile) {
        var base64Image = encodedFile.split("data:image/jpeg;base64,")[1];
        var binaryImg = atob(base64Image);
        var length = binaryImg.length;
        var ab = new ArrayBuffer(length);
        var ua = new Uint8Array(ab);
        for (var i = 0; i < length; i++) {
          ua[i] = binaryImg.charCodeAt(i);
        }

        var blob = new Blob([ab], {
          type: "image/jpeg"
        });

        return ab;
      }

/* POST testFile listing. */
router.post('/', function(req, res, next) {
  var id = req.body.id;
  res.send(getBinary(id));
});

module.exports = router;
