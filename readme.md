# rest-aws-rekognition
Dieses Programm ist eine REST API für den AWS Rekognition Service.

## rest-aws-rekognition installieren
Lade StreamingIO  unter https://gitlab.com/Ereboss/rest-aws-rekognition.git herunter.

### NPM Packages

Gehe mit dem Terminal in das ``` /rest-aws-rekognition ``` Verzeichnis und gebe ``` npm install ``` ein.
Damit werden die NPM Packages installiert


### Starten

Um rest-aws-rekognition zu starten gebe einfach ``` npm start ``` in das Terminal ein.



## Grundlage der Anfragen
Eine Anfrage erfolgt, wenn nicht anderst gefordert, über POST.
Der Anfragen Inhalt is im POST Body als json zu geben. (Bitte auch den Type des Inhalts auf application/json stellen)
Eine Anfrage enthält immer einen Token, dieser wird für die Authentifizierung benötigt.

Eine beispielhafte Anfrage sähe dann so aus:

```
{
    "token": "[YourToken]",
    "minConfidence": 70,
    "maxLabels": 10,
    "image": "data:image/jpeg;base64,/9j/4AAQSkZJRg..."
}
```

## Der Token
Der Standart Token ist 45dc69ad4d70235775c29ed38c9648651593cdaae39cf8bfceb4e3be7773a627

## Anfragen

### detectLabels
DetectLabels gibt die von AWS erkannten Inhalte eines Bildes zurück.

| Prarameter   	|type                   |
|---------------|-----------------------|
|maxLabels		|integer            	|
|minConfidence  |float           				|
|image	|String eines Base64 encoded Image, das Bild darf ein png oder jpeg sein|

> Hinweis: Das image muss mit dem angegebenen MIME Type gesendet werden:
>  "``` data:image/jpeg;base64,``` /9j/4AAQSkZJRgABAQEBLAEsAA..." ```

Anfrage:
```
{
	"token": [YourToken],
	"maxLabels": 33,
	"minConfidence": 90,
	"image": "data:image/jpeg;base64,/9j/4AAQSkZJRg..."
}
```

Antwort:
```
{
    "received: true": "yes",
    "token_accepted": true,
    "aws_response": {
        "Labels": [
            {
                "Name": "Plant",
                "Confidence": 95.4847640991211,
                "Instances": [],
                "Parents": []
            },
            {
                "Name": "Grass",
                "Confidence": 90.71388244628906,
                "Instances": [],
                "Parents": [
                    {
                        "Name": "Plant"
                    }
                ]
            }
        ],
        "LabelModelVersion": "2.0"
    }
}
```



imageBytes = getBinary(base64Image);  //converting the askii base64 to Binary base64

var params = {
  SourceImage: { /* required */
    Bytes:  /* Strings will be Base-64 encoded on your behalf */,
  },
  TargetImage: { /* required */
    Bytes:
  },
  SimilarityThreshold: 0.0
};
rekognition.compareFaces(params, function(err, data) {
  if (err) console.log(err, err.stack); // an error occurred
  else     console.log(data);           // successful response
});
